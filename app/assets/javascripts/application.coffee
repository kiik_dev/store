#= require jquery
#= require jquery_ujs
#= require twitter/bootstrap
#= require_tree
#= require websocket_rails/main

$ ->
  $('form.disableable').on 'submit', ->
    $('input[type=submit]').addClass('disabled').prop('disabled', true)

  addOverlay = (onClose)->
    $('div.overlay').remove()
    $("<div class='overlay'></div>").appendTo('body').on 'click', ->
      $(this).remove()
      onClose()


  $('div.product div.optionsBox div.links li.item.box a').on 'click', ->
    item = $('div.product div.optionsBox div.in_the_box').toggle()
    addOverlay -> item.hide()
    false

  toggleDeliveryAddress = ->
    if $('div.checkout input#order_separate_delivery_address').prop('checked')
      $('div.checkout dl.delivery').show()
    else
      $('div.checkout dl.delivery').hide()
    false
  $('div.checkout input#order_separate_delivery_address').on 'change', toggleDeliveryAddress
  toggleDeliveryAddress() if $('div.checkout').length

  #
  # When clicking links in the order items table, submit them
  # using the ajaxLink helper
  #
  $('body').on 'click', 'table.orderItems tbody td a.ajax', -> ajaxLink.call(this, updateOrderItemsFromRemote)

  #
  # When the delivery method is changed on the form, submit the associated
  # form with ajax
  #
  $('body').on 'change', 'table.orderItems select', ->
    form = $(this).parents('form')
    $.ajax
      url: form.attr('action')
      type: form.attr('method')
      data: form.serialize()
      dataType: 'json'
      success: updateOrderItemsFromRemote

  $('#pay_now_link').click ->
    $.ajax(
      url: '/checkout/pay-now-kiik'
      type: 'GET'
      data: {}
      dataType: 'html'
      success: (data) ->
        console.log data
        $('#payment_alert').html(data)
        $('#pay_now_modal').modal('show')
      error: (data)->
    )


  # uri = window.document.location.host + "/websocket";
  # dispatcher = new WebSocketRails(uri)
  # dispatcher.on_open = (data) ->
  #   console.log 'Connection has been established: ', data
  # dispatcher.bind 'order_finished', (data) ->
  #   #here the magic happens
  #   alert data.name + "pagou o que devia"
