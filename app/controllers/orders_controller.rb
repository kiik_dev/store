# coding: utf-8
require 'httparty'
include ActionView::Helpers::NumberHelper

class OrdersController < ApplicationController
  include ActiveMerchant::Billing
  include PaypalExpressHelper
  include WebsocketRails

  before_action :authenticate

  def show
    @hide_header = true
  end

  def wizard
    @hide_header = true
    unless current_order.order_items.any?
      flash[:notice] = 'Carrinho vazio! (:'
      redirect_to root_path
    end
    payment
  end

  def checkout
    @order = current_order
    @hide_header = true
    if request.patch? #&& @order.proceed_to_confirm(order_params)
      redirect_to checkout_payment_path
    end
  end

  def payment
    @code = create_order(current_order)
  end

  def create_order(current_order)
    return if current_order.order_items.empty?
    @code = current_order.kiik_token
    return @code if @code
    items = current_order.order_items.map do |item|
      {
        product_description: item.ordered_item.name,
        price_unit: item.ordered_item.price.to_s,
        quant: item.quantity
      }
    end

    header = {
      Accept: 'application/json'
    }

    auth = {
      username: '8210882a5815600efc6090455e3b1178228cebd7',
      password: ''
    }

    body = {
      order: {
        metadata: {
          callback_url: "http://#{request.headers['HTTP_HOST']}/checkout/finish-order",
          items: items
        },
        amount: items.map { |x| x[:price_unit] }.sum.to_f * 100,
        company_identifier: "010477"
      }
    }


    url = 'http://staging-backend.kiik.com//api/v2/orders'

    response = HTTParty.post(url, header: header, body: body, basic_auth: auth)

    unless response.success?
      flash[:alert] = 'uh oh, houve um erro com a comunicação com o Kiik ):'
      return redirect_to checkout_path
    end
    current_order.update(kiik_token: response["order"]["code"])
    response["order"]["code"]
  end

  def billet
    @order = current_order
  end

  def paypal
    @hide_header = true
  end

  def card_information
    @hide_header = true
    @order = current_order
  end

  def redirect_paypal
    @order = current_order

    items = @order.order_items.map do |item|
      {
        name: item.ordered_item.name,
        description: item.ordered_item.description,
        quantity: item.quantity,
        amount: to_cent(item.ordered_item.price)
      }
    end

    response = EXPRESS_GATEWAY.setup_purchase(to_cent(@order.total),
      items: items,
      ip: request.remote_ip,
      return_url: confirmation_url,
      cancel_return_url: catalogue_url,
      brand_name: 'Kiik::Store',
      cart_border_color: '000CD')
    unless response.success?
      flash[:alert] = 'uh oh, houve um erro com a comunicação com o Paypal ):'
      return redirect_to checkout_path
    end

    redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end

  def finalize
    @hide_header = false
    reset_session
    current_order = nil

    render partial: 'orders/finalize', layout: false
  end

  def finish_order
    current_order = Shoppe::Order.find_by_kiik_token(params[:id])
    WebsocketRails.users[$redis.get(current_order.id)].send_message :order_finished, params[:order]
    render html: "ok".html_safe
  end

  def remove_item
    current_order_item = Shoppe::OrderItem.find(params[:order_item_id])
    current_order_item.destroy

    return redirect_to basket_path if current_order.order_items.any?
    redirect_to root_path
  end

  def destroy
    @current_order.destroy
    session[:order_id] = nil
    redirect_to root_path, notice: 'Basket Empty (:'
  end

  def change_item_quantity
    order_item_id = params['order_item_id']
    quantity = params['quantity']
    order_item = current_order.order_items.find(order_item_id)
    order_item.quantity = quantity
    order_item.save!

    total = number_to_currency(order_item.order.total, unit: "R$", separator: ",", delimiter: ".")

    render html: "#{total}".html_safe
  end

  def update_order
  end

  def pay_now
    @code = create_order(current_order)
    respond_to do |format|
      format.json { return render(partial: 'orders/pay_now', layout: false, formats: [:html])}
    end
  end

  private

  def order_params
    params[:order].permit(
      :first_name,
      :last_name,
      :billing_address1,
      :billing_address2,
      :billing_address3,
      :billing_address4,
      :billing_country_id,
      :billing_postcode,
      :email_address,
      :phone_number
    )
  end

  def create_payment(order)
    544515
  end
end
