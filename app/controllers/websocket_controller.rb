class WebsocketController < WebsocketRails::BaseController
  def initialize_session
    # perform application setup here
    controller_store[:message_count] = 0
  end

  def user_connected
    p 'user connected'
    WebsocketRails.users[client_id] = connection
    $redis.set(session[:order_id], client_id)
  end
end
