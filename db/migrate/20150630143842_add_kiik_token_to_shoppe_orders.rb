class AddKiikTokenToShoppeOrders < ActiveRecord::Migration
  def change
    add_column :shoppe_orders, :kiik_token, :string
  end
end
